package com.tooploox.weatherdroid.event;

import com.tooploox.weatherdroid.rest.model.weather.DayWeather;

/**
 * Created by Adam on 28.05.15 --|
 */
public class ShowInfoEventBus {
    private DayWeather mDayWeather;

    public ShowInfoEventBus(DayWeather dayWeather) {
        mDayWeather = dayWeather;
    }

    public DayWeather getDayWeather() {
        return mDayWeather;
    }
}

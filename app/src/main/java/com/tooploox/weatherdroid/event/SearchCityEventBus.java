package com.tooploox.weatherdroid.event;

import com.tooploox.weatherdroid.rest.model.search.City;

/**
 * Created by Adam on 28.05.15 --|
 */
public class SearchCityEventBus {
    private City mCity;

    public SearchCityEventBus(City city) {
        mCity = city;
    }

    public City getCity() {
        return mCity;
    }
}

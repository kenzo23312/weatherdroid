package com.tooploox.weatherdroid;

import android.app.Application;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.squareup.otto.Bus;

/**
 * Created by Adam on 26.05.15 --|
 */
public class WeatherApp extends Application {

    private static Bus sBus;

    @Override
    public void onCreate() {
        super.onCreate();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        sBus = new Bus();
    }

    public static Bus getBus() {
        return sBus;
    }
}

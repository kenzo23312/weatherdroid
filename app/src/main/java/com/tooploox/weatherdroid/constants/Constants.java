package com.tooploox.weatherdroid.constants;

/**
 * Created by Adam on 26.05.15 --|
 */
public final class Constants {
    public static final String DEFAULT_CITY_NAME = "Wroclaw";
    public static final int DEFAULT_DAY_DURATION = 5;
}

package com.tooploox.weatherdroid.rest.service.parameter;

import com.tooploox.weatherdroid.rest.service.constants.ServiceConstants;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Adam on 25.05.15 --|
 */
public class ParameterBuilder {
    private final static String KEY_PARAMETER = "key";
    private final static String FORMAT_PARAMETER = "format";

    private static ParameterBuilder mParameterBuilder;
    private static Map<String, String> mParameters;

    private ParameterBuilder() { }

    public static ParameterBuilder getInstance() {
        if(mParameterBuilder == null) {
            mParameters = new HashMap<>();
            mParameterBuilder = new ParameterBuilder();
        } else {
            mParameters.clear();
        }

        mParameters.put(KEY_PARAMETER, ServiceConstants.API_KEY);
        mParameters.put(FORMAT_PARAMETER, ServiceConstants.API_FORMAT);

        return mParameterBuilder;
    }

    public void addParameter(Parameter parameter, String value) {
        mParameters.put(parameter.toString(), value);
    }

    public Map<String, String> getParameters() {
        return mParameters;
    }
}

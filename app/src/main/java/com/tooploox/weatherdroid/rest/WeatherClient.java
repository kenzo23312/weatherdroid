package com.tooploox.weatherdroid.rest;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.tooploox.weatherdroid.rest.model.search.CityResponse;
import com.tooploox.weatherdroid.rest.model.weather.WeatherResponse;
import com.tooploox.weatherdroid.rest.service.WeatherApiService;
import com.tooploox.weatherdroid.rest.service.constants.ServiceConstants;
import com.tooploox.weatherdroid.rest.service.parameter.Parameter;
import com.tooploox.weatherdroid.rest.service.parameter.ParameterBuilder;

import java.util.HashMap;
import java.util.Map;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.converter.GsonConverter;

/**
 * Created by Adam on 25.05.15 --|
 */
public class WeatherClient {
    private static WeatherApiService mWeatherApiService;
    private static WeatherClient mWeatherClient;

    private WeatherClient () { }

    public static WeatherClient getInstance() {
        if(mWeatherClient == null) {
            mWeatherClient = new WeatherClient();

            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'SSS'Z'")
                    .create();

            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .setEndpoint(ServiceConstants.API_URL)
                    .setConverter(new GsonConverter(gson))
                    .build();

            mWeatherApiService = restAdapter.create(WeatherApiService.class);
        }

        return mWeatherClient;
    }

    public void getWeatherForCity(HashMap<Parameter, String> parameters, Callback<WeatherResponse> callback) {
        mWeatherApiService.getLocalWeather(buildParameters(parameters), callback);
    }

    public void getCities(HashMap<Parameter, String> parameters, Callback<CityResponse> callback) {
        mWeatherApiService.getCities(buildParameters(parameters), callback);
    }

    private Map<String, String> buildParameters(HashMap<Parameter, String> parameters) {
        ParameterBuilder parameterBuilder = ParameterBuilder.getInstance();

        for(Parameter wp : parameters.keySet()) {
            parameterBuilder.addParameter(wp, parameters.get(wp));
        }

        return parameterBuilder.getParameters();
    }
}

package com.tooploox.weatherdroid.rest.model.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Adam on 26.05.15 --|
 */
public class City {
    @SerializedName("latitude")
    private double mLatitude;

    @SerializedName("longitude")
    private double mLongitude;

    @SerializedName("areaName")
    private List<Area> mArea;

    @SerializedName("country")
    private List<Country> mCountries;

    public double getLatitude() {
        return mLatitude;
    }

    public double getLongitude() {
        return mLongitude;
    }

    public List<Area> getAreas() {
        return mArea;
    }

    public List<Country> getCountries() {
        return mCountries;
    }

    public String getLocationQuery() {
        return mLatitude + "," + mLongitude;
    }
}

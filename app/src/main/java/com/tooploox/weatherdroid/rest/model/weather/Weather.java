package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Adam on 25.05.15 --|
 */
public class Weather {
    @SerializedName("current_condition")
    private List<CurrentCondition> mCurrentCondition;

    @SerializedName("weather")
    private List<DayWeather> mDayWeather;

    public List<CurrentCondition> getCurrentCondition() {
        return mCurrentCondition;
    }

    public List<DayWeather> getDayWeather() {
        return mDayWeather;
    }
}

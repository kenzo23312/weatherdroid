package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 26.05.15 --|
 */
public class WeatherIcon {

    @SerializedName("value")
    private String mIconUrl;

    public String getIconUrl() {
        return  mIconUrl;
    }
}

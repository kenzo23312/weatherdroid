package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 25.05.15 --|
 */
public class WeatherResponse {

    @SerializedName("data")
    private Weather mWeather;

    public Weather getWeather() {
        return  mWeather;
    }
}

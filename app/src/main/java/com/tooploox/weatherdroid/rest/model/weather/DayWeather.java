package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by Adam on 25.05.15 --|
 */
public class DayWeather implements Serializable {
    @SerializedName("date")
    private String mDate;

    @SerializedName("maxtempC")
    private int mMaxTemperatureC;

    @SerializedName("mintempC")
    private int mMinTemperatureC;

    @SerializedName("astronomy")
    private List<Astronomy> mAstronomy;

    public int getMinTemperatureC() {
        return mMinTemperatureC;
    }

    public String getDate() {
        return mDate;
    }

    public int getMaxTemperatureC() {
        return mMaxTemperatureC;
    }

    public List<Astronomy> getAstronomy() {
        return mAstronomy;
    }
}

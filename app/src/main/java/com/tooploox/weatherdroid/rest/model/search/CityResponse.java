package com.tooploox.weatherdroid.rest.model.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 26.05.15 --|
 */
public class CityResponse {
    @SerializedName("search_api")
    private CityResult mCityResult;

    public CityResult getResult() {
        return mCityResult;
    }
}

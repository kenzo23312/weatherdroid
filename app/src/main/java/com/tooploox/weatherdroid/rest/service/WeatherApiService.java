package com.tooploox.weatherdroid.rest.service;

import com.tooploox.weatherdroid.rest.model.search.CityResponse;
import com.tooploox.weatherdroid.rest.model.weather.WeatherResponse;
import com.tooploox.weatherdroid.rest.service.constants.ServiceConstants;

import java.util.Map;

import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.QueryMap;

/**
 * Created by Adam on 25.05.15 --|
 */
public interface WeatherApiService {

    @GET(ServiceConstants.API_WEATHER_LOCAL)
    public void getLocalWeather(@QueryMap Map<String, String> parameters, Callback<WeatherResponse> callback);

    @GET(ServiceConstants.API_SEARCH)
    public void getCities(@QueryMap Map<String, String> parameters, Callback<CityResponse> callback);
}

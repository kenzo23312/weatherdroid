package com.tooploox.weatherdroid.rest.model.search;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Adam on 26.05.15 --|
 */
public class CityResult {
    @SerializedName("result")
    private List<City> mCityList;

    public List<City> getCityList() {
        return mCityList;
    }
}

package com.tooploox.weatherdroid.rest.model.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 26.05.15 --|
 */
public class Area {
    @SerializedName("value")
    private String mAreaNAme;

    public String getAreaName() {
        return mAreaNAme;
    }
}

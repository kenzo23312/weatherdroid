package com.tooploox.weatherdroid.rest.service.parameter;

/**
 * Created by Adam on 25.05.15 --|
 */
public enum WeatherParameter implements Parameter {
    CITY ("q"),
    DURATION("num_of_days");

    private String mName;

    WeatherParameter(String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return mName;
    }
}

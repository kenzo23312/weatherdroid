package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Adam on 25.05.15 --|
 */
public class CurrentCondition {

    @SerializedName("cloudcover")
    private int mCloudCover;

    @SerializedName("temp_C")
    private int mTemperatureC;

    @SerializedName("humidity")
    private int mHumidity;

    @SerializedName("observation_time")
    private String mObservationTime;

    @SerializedName("weatherIconUrl")
    private List<WeatherIcon> mIconUrls;

    public int getCloudCover() {
        return mCloudCover;
    }

    public int getTemperatureC() {
        return mTemperatureC;
    }

    public int getHumidity() {
        return mHumidity;
    }

    public List<WeatherIcon> getIconUrls() {
        return mIconUrls;
    }

    public String getObservationTime() {
        return mObservationTime;
    }

    @Override
    public String toString() {
        return "DayWeather{" +
                "mCloudCover=" + mCloudCover +
                ", mTemperatureC=" + mTemperatureC +
                ", mHumidity=" + mHumidity +
                '}';
    }
}

package com.tooploox.weatherdroid.rest.service.parameter;

/**
 * Created by Adam on 25.05.15 --|
 */
public enum SearchParameter {
    CITY ("q"),
    DURATION("num_of_days");

    private String mName;

    SearchParameter(String name) {
        mName = name;
    }

    @Override
    public String toString() {
        return mName;
    }
}

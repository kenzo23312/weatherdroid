package com.tooploox.weatherdroid.rest.model.weather;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 28.05.15 --|
 */
public class Astronomy {
    @SerializedName("moonrise")
    private String mMoonrise;

    @SerializedName("moonset")
    private String mMoonset;

    @SerializedName("sunrise")
    private String mSunrise;

    @SerializedName("sunset")
    private String mSunset;

    public String getSunset() {
        return mSunset;
    }

    public String getMoonrise() {
        return mMoonrise;
    }

    public String getMoonset() {
        return mMoonset;
    }

    public String getSunrise() {
        return mSunrise;
    }
}

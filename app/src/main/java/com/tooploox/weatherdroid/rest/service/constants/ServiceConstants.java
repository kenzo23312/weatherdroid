package com.tooploox.weatherdroid.rest.service.constants;

/**
 * Created by Adam on 25.05.15 --|
 */
public final class ServiceConstants {
    public static final String API_URL = "http://api.worldweatheronline.com/free/v2";

    public static final String API_WEATHER_LOCAL = "/weather.ashx";
    public static final String API_SEARCH = "/search.ashx";

    public static final String API_KEY = "4499377f42d5a929c615bf02e3d92";
    public static final String API_FORMAT = "json";

}

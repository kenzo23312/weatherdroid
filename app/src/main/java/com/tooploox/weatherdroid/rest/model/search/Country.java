package com.tooploox.weatherdroid.rest.model.search;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Adam on 26.05.15 --|
 */
public class Country {
    @SerializedName("value")
    private String mCountryName;

    public String getCountryName() {
        return  mCountryName;
    }
}

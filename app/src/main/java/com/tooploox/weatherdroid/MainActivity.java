package com.tooploox.weatherdroid;

import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.squareup.otto.Subscribe;
import com.tooploox.weatherdroid.event.SearchCityEventBus;
import com.tooploox.weatherdroid.event.ShowInfoEventBus;
import com.tooploox.weatherdroid.fragment.SearchFragment;
import com.tooploox.weatherdroid.fragment.WeatherCityFragment;
import com.tooploox.weatherdroid.fragment.WeatherInfoFragment;
import com.tooploox.weatherdroid.rest.model.weather.DayWeather;

/**
 *
 * MainActivity class contain logic for commiting and replacing fragments.
 *
 * @author Adam Podsiadło
 *
 */

public class MainActivity extends BaseActivity {
    private static final String TAG_WEATHER_INFO = "TAG_WEATHER_INFO";
    private static final String TAG_WEATHER_CITY = "TAG_WEATHER_CITY";
    private static final String LOG_TAG = MainActivity.class.getSimpleName();

    private MenuItem mSearchItem;
    private SearchView mSvCity;

    private WeatherCityFragment mWeatherCityFragment;
    private SearchFragment mSearchFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getFragmentOnConfigurationChanged(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        mSearchItem = menu.findItem(R.id.action_search);
        mSvCity = (SearchView) mSearchItem.getActionView();

        setMenuActionsListener();

        return super.onCreateOptionsMenu(menu);
    }

    @Subscribe
    public void onShowInfo(ShowInfoEventBus showInfoEventBus) {
        commitWeatherInfoFragment(showInfoEventBus.getDayWeather());
    }

    @Subscribe
    public void onSearchCity(SearchCityEventBus searchCityEventBus) {
        mWeatherCityFragment.setCity(searchCityEventBus.getCity());

        mSearchItem.collapseActionView();
        getSupportFragmentManager().popBackStack();
    }

    private void setMenuActionsListener() {
        mSvCity.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String s) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String s) {
                mSearchFragment.findCities(s);

                return false;
            }
        });

        MenuItemCompat.setOnActionExpandListener(mSearchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                getSupportFragmentManager().popBackStack();

                return true;
            }

            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (getSupportFragmentManager().findFragmentByTag(TAG_WEATHER_INFO) != null) {
                    getSupportFragmentManager().popBackStack();
                }

                commitSearchFragment();

                return true;
            }
        });
    }

    private void getFragmentOnConfigurationChanged(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            commitWeatherCityFragment();
        } else {
            mWeatherCityFragment = (WeatherCityFragment) getSupportFragmentManager().findFragmentByTag(TAG_WEATHER_CITY);
        }
    }

    private void commitWeatherCityFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        mWeatherCityFragment = new WeatherCityFragment();

        fragmentTransaction.add(R.id.fmContent, mWeatherCityFragment, TAG_WEATHER_CITY);
        fragmentTransaction.commit();
    }

    private void commitWeatherInfoFragment(DayWeather dayWeather) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        WeatherInfoFragment weatherInfoFragment = WeatherInfoFragment.getInstance(dayWeather);

        fragmentTransaction.replace(R.id.fmContent, weatherInfoFragment, TAG_WEATHER_INFO).addToBackStack(null);
        fragmentTransaction.commit();
    }

    private void commitSearchFragment() {
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        mSearchFragment = new SearchFragment();

        fragmentTransaction.replace(R.id.fmContent, mSearchFragment, null).addToBackStack(null);
        fragmentTransaction.commit();
    }
}

package com.tooploox.weatherdroid.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tooploox.weatherdroid.R;
import com.tooploox.weatherdroid.rest.model.weather.DayWeather;

import java.util.List;

/**
 * Created by Adam on 26.05.15 --|
 */
public class DayWeatherAdapter extends RecyclerView.Adapter<DayWeatherAdapter.DayWeatherViewHolder> {

    public static class DayWeatherViewHolder extends RecyclerView.ViewHolder {
        private TextView txtvDate;
        private TextView txtvTempMinC;
        private TextView txtvTempMaxC;


        public DayWeatherViewHolder(View view) {
            super(view);

            txtvDate =  (TextView) view.findViewById(R.id.txtvDate);
            txtvTempMinC =  (TextView) view.findViewById(R.id.txtvMinC);
            txtvTempMaxC =  (TextView) view.findViewById(R.id.txtvMaxC);
        }
    }

    private List<DayWeather> mData;

    public DayWeatherAdapter(List<DayWeather> dayWeather) {
        mData = dayWeather;
    }

    public void addItem(DayWeather dayWeather) {
        if(!mData.contains(dayWeather)) {
            mData.add(dayWeather);
            notifyItemInserted(mData.indexOf(dayWeather));
        }
    }

    public void removeItem(DayWeather dayWeather) {
        if(mData.contains(dayWeather)) {
            int position = mData.indexOf(dayWeather);

            mData.remove(dayWeather);
            notifyItemRemoved(position);
        }
    }

    @Override
    public DayWeatherViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.day_weather_item, viewGroup, false);
        DayWeatherViewHolder dayWeatherViewHolder = new DayWeatherViewHolder(v);

        return dayWeatherViewHolder;
    }

    @Override
    public void onBindViewHolder(DayWeatherViewHolder dayWeatherViewHolder, int i) {
        DayWeather dayWeather = mData.get(i);

        String tempMin = dayWeatherViewHolder.txtvTempMaxC.getContext().getResources().getString(R.string.temperature_min);
        String tempMax = dayWeatherViewHolder.txtvTempMaxC.getContext().getResources().getString(R.string.temperature_max);

        dayWeatherViewHolder.txtvDate.setText(dayWeather.getDate());
        dayWeatherViewHolder.txtvTempMinC.setText(tempMin + " " + dayWeather.getMinTemperatureC() + "");
        dayWeatherViewHolder.txtvTempMaxC.setText(tempMax + " " + dayWeather.getMaxTemperatureC() + "");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}

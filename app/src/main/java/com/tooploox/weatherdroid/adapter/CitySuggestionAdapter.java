package com.tooploox.weatherdroid.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.tooploox.weatherdroid.R;
import com.tooploox.weatherdroid.rest.model.search.City;

import java.util.List;

/**
 * Created by Adam on 28.05.15 --|
 */
public class CitySuggestionAdapter extends RecyclerView.Adapter<CitySuggestionAdapter.CitySuggestionViewHolder> {

    public static class CitySuggestionViewHolder extends RecyclerView.ViewHolder {
        private TextView txtvCity;
        private TextView txtvCountry;
        private TextView txtvLocation;

        public CitySuggestionViewHolder(View view) {
            super(view);

            txtvCity =  (TextView) view.findViewById(R.id.txtvCity);
            txtvCountry =  (TextView) view.findViewById(R.id.txtvCountry);
            txtvLocation =  (TextView) view.findViewById(R.id.txtvPopulation);
        }
    }

    private List<City> mData;

    public CitySuggestionAdapter(List<City> city) {
        mData = city;
    }

    @Override
    public CitySuggestionViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.city_suggestion_item, viewGroup, false);
        CitySuggestionViewHolder citySuggestionViewHolder = new CitySuggestionViewHolder(v);

        return citySuggestionViewHolder;
    }

    @Override
    public void onBindViewHolder(CitySuggestionViewHolder citySuggestionViewHolder, int i) {
        City city = mData.get(i);

        if(city.getAreas().size() > 0) {
            citySuggestionViewHolder.txtvCity.setText(city.getAreas().get(0).getAreaName());
        }

        if(city.getCountries().size() > 0) {
            citySuggestionViewHolder.txtvCountry.setText(city.getCountries().get(0).getCountryName());
        }

        String location = citySuggestionViewHolder.txtvLocation.getContext().getResources().getString(R.string.location);
        citySuggestionViewHolder.txtvLocation.setText(location + " (" + city.getLatitude() + "; " + city.getLongitude() + ")");
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }
}

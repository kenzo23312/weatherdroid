package com.tooploox.weatherdroid;

import android.support.v7.app.ActionBarActivity;

import butterknife.ButterKnife;

/**
 * Created by Adam on 26.05.15 --|
 */
public class BaseActivity extends ActionBarActivity {

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);

        ButterKnife.inject(this);

        WeatherApp.getBus().register(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        WeatherApp.getBus().unregister(this);
    }
}

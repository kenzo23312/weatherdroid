package com.tooploox.weatherdroid.fragment;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.tooploox.weatherdroid.R;
import com.tooploox.weatherdroid.event.ShowInfoEventBus;
import com.tooploox.weatherdroid.WeatherApp;
import com.tooploox.weatherdroid.adapter.DayWeatherAdapter;
import com.tooploox.weatherdroid.constants.Constants;
import com.tooploox.weatherdroid.listener.RecyclerItemClickListener;
import com.tooploox.weatherdroid.rest.WeatherClient;
import com.tooploox.weatherdroid.rest.model.search.City;
import com.tooploox.weatherdroid.rest.model.weather.CurrentCondition;
import com.tooploox.weatherdroid.rest.model.weather.DayWeather;
import com.tooploox.weatherdroid.rest.model.weather.WeatherResponse;
import com.tooploox.weatherdroid.rest.service.parameter.Parameter;
import com.tooploox.weatherdroid.rest.service.parameter.WeatherParameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 *
 * WeatherCityFragment class presents info about current weather in city with list
 * for upcoming days.
 *
 * @author Adam Podsiadło
 *
 */

public class WeatherCityFragment extends BaseFragment {
    private static final String LOG_TAG = WeatherCityFragment.class.getSimpleName();

    @InjectView(R.id.txtvCityName) TextView mTxtvCityName;
    @InjectView(R.id.txtvTemperature) TextView mTxtvTemperature;
    @InjectView(R.id.txtvHumidity) TextView mTxtvHumidity;
    @InjectView(R.id.txtvDuration) TextView mTxtvDuration;
    @InjectView(R.id.txtvObservationTime) TextView mTxtvObservationTime;

    @InjectView(R.id.imgvWeatherIcon) ImageView mImgvIcon;

    @InjectView(R.id.rvDayWeather) RecyclerView mRvDayWeather;

    @InjectView(R.id.sbDuration) SeekBar mSbDays;

    @InjectView(R.id.pbLoader) ProgressBar mPbLoader;

    private DisplayImageOptions mDisplayImageOptionsIcons;

    private List<DayWeather> mDayWeather;

    private DayWeatherAdapter mDayWeatherAdapter;

    private City mCurrentCity;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setRetainInstance(true);

        initData();
        setListeners();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_weather_city;
    }

    public void setCity(City city) {
        mCurrentCity = city;
    }

    private void getWeatherForCity(City city) {
        mCurrentCity = city;

        mTxtvCityName.setText("");
        mTxtvHumidity.setText("");
        mTxtvTemperature.setText("");
        mImgvIcon.setImageBitmap(null);

        mPbLoader.setVisibility(View.VISIBLE);

        mDayWeather = new ArrayList<>();
        mDayWeatherAdapter = new DayWeatherAdapter(mDayWeather);
        mRvDayWeather.setAdapter(mDayWeatherAdapter);

        getWeatherForCity(city.getLocationQuery(), Constants.DEFAULT_DAY_DURATION);

        if(city.getAreas().size() > 0) {
            mTxtvCityName.setText(city.getAreas().get(0).getAreaName());
        }
    }

    private void initData() {
        mDisplayImageOptionsIcons = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisk(true)
                .considerExifParams(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRvDayWeather.setLayoutManager(layoutManager);
        mDayWeather = new ArrayList<>();

        mDayWeatherAdapter = new DayWeatherAdapter(mDayWeather);
        mRvDayWeather.setAdapter(mDayWeatherAdapter);

        mTxtvDuration.setText(getResources().getString(R.string.weather_for) + Constants.DEFAULT_DAY_DURATION + " " + getResources().getString(R.string.days));

        if(mCurrentCity != null) {
            getWeatherForCity(mCurrentCity);
        } else {
            mTxtvCityName.setText(Constants.DEFAULT_CITY_NAME);
            getWeatherForCity(Constants.DEFAULT_CITY_NAME, Constants.DEFAULT_DAY_DURATION);
        }
    }

    private void setListeners() {
        mSbDays.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (i == 0) {
                    mTxtvDuration.setText(getResources().getString(R.string.weather_for) + (i + 1) + " " + getResources().getString(R.string.day));
                } else {
                    mTxtvDuration.setText(getResources().getString(R.string.weather_for) + (i + 1) + " " + getResources().getString(R.string.days));
                }

                setWeatherForNextDays(i + 1);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        mRvDayWeather.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                WeatherApp.getBus().post(new ShowInfoEventBus(mDayWeather.get(position)));
            }
        }));
    }

    private void getWeatherForCity(String city, int duration) {
        WeatherClient weatherClient = WeatherClient.getInstance();

        HashMap<Parameter, String> parameters = new HashMap<>();

        parameters.put(WeatherParameter.CITY, city);
        parameters.put(WeatherParameter.DURATION, duration + "");

        weatherClient.getWeatherForCity(parameters, new Callback<WeatherResponse>() {
            @Override
            public void success(WeatherResponse weatherResponse, Response response) {
                if (weatherResponse.getWeather().getCurrentCondition().size() > 0) {
                    setCurrentCondition(weatherResponse.getWeather().getCurrentCondition().get(0));
                }

                if (weatherResponse.getWeather().getDayWeather().size() > 0) {
                    mDayWeather = weatherResponse.getWeather().getDayWeather();

                    setWeatherForNextDays(Constants.DEFAULT_DAY_DURATION);
                }

                mPbLoader.setVisibility(View.GONE);
            }

            @Override
            public void failure(RetrofitError error) {
                mTxtvCityName.setText(getResources().getString(R.string.error_loading));
                mPbLoader.setVisibility(View.GONE);
            }
        });
    }

    private void setWeatherForNextDays(int duration) {
        for(int i = 0; i < mDayWeather.size(); i++) {
            if(i < duration) {
                mDayWeatherAdapter.addItem(mDayWeather.get(i));
            } else {
                mDayWeatherAdapter.removeItem(mDayWeather.get(i));
            }
        }
    }

    private void setCurrentCondition(CurrentCondition currentCondition) {
        mTxtvTemperature.setText(getString(R.string.temperature) + currentCondition.getTemperatureC() + "");
        mTxtvHumidity.setText(getString(R.string.humidity) + currentCondition.getHumidity() + "");
        mTxtvObservationTime.setText(getResources().getString(R.string.observation_time) + currentCondition.getObservationTime());

        if(currentCondition.getIconUrls().size() > 0) {
            ImageLoader.getInstance().displayImage(currentCondition.getIconUrls().get(0).getIconUrl(),
                    mImgvIcon, mDisplayImageOptionsIcons, new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String s, View view) { }

                @Override
                public void onLoadingFailed(String s, View view, FailReason failReason) {
                    mImgvIcon.setImageResource(R.drawable.abc_ic_clear_mtrl_alpha);
                }

                @Override
                public void onLoadingComplete(String s, View view, Bitmap bitmap) { }

                @Override
                public void onLoadingCancelled(String s, View view) { }
            });
        }
    }
}

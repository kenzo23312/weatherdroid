package com.tooploox.weatherdroid.fragment;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.tooploox.weatherdroid.R;
import com.tooploox.weatherdroid.rest.model.weather.DayWeather;

import butterknife.InjectView;

/**
 *
 * WeatherInfoFragment class presents info about current day.
 *
 * @author Adam Podsiadło
 *
 */

public class WeatherInfoFragment extends BaseFragment {
    private static final String DAY_WEATHER = "day_weather";

    @InjectView(R.id.txtvDate) TextView mTxtvDate;
    @InjectView(R.id.txtvTemperatureMin) TextView mTxtvTemperatureMin;
    @InjectView(R.id.txtvTemperatureMax) TextView mTxtvTemperatureMax;
    @InjectView(R.id.txtvMoonrise) TextView mTxtvMoonrise;
    @InjectView(R.id.txtvMoonset) TextView mTxtvMoonset;
    @InjectView(R.id.txtvSunrise) TextView mTxtvSunrise;
    @InjectView(R.id.txtvSunset) TextView mTxtvSunset;

    private DayWeather mDayWeather;

    public static WeatherInfoFragment getInstance(DayWeather dayWeather) {
        WeatherInfoFragment weatherInfoFragment = new WeatherInfoFragment();

        Bundle args = new Bundle();
        args.putSerializable(DAY_WEATHER, dayWeather);
        weatherInfoFragment.setArguments(args);

        return  weatherInfoFragment;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setRetainInstance(true);

        mDayWeather = getArguments() != null ? (DayWeather) getArguments().getSerializable(DAY_WEATHER) : null;

        initData();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_weather_info;
    }

    private void initData() {
        if(mDayWeather != null) {
            mTxtvDate.setText(getResources().getString(R.string.date) + mDayWeather.getDate());
            mTxtvTemperatureMin.setText(getResources().getString(R.string.temperature_min) + mDayWeather.getMinTemperatureC() + "");
            mTxtvTemperatureMax.setText(getResources().getString(R.string.temperature_max) + mDayWeather.getMaxTemperatureC() + "");

            if (mDayWeather.getAstronomy().size() > 0) {
                mTxtvMoonrise.setText(getResources().getString(R.string.moonrise) + mDayWeather.getAstronomy().get(0).getMoonrise());
                mTxtvMoonset.setText(getResources().getString(R.string.moonset) + mDayWeather.getAstronomy().get(0).getMoonset());
                mTxtvSunrise.setText(getResources().getString(R.string.sunrise) + mDayWeather.getAstronomy().get(0).getSunrise());
                mTxtvSunset.setText(getResources().getString(R.string.sunset) + mDayWeather.getAstronomy().get(0).getSunset());
            } else {
                mTxtvMoonrise.setText(getResources().getString(R.string.moonrise) + "-");
                mTxtvMoonset.setText(getResources().getString(R.string.moonset) + "-");
                mTxtvSunrise.setText(getResources().getString(R.string.sunrise) + "-");
                mTxtvSunset.setText(getResources().getString(R.string.sunset) + "-");
            }
        }
    }
}

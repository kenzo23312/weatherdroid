package com.tooploox.weatherdroid.fragment;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.tooploox.weatherdroid.R;
import com.tooploox.weatherdroid.WeatherApp;
import com.tooploox.weatherdroid.adapter.CitySuggestionAdapter;
import com.tooploox.weatherdroid.event.SearchCityEventBus;
import com.tooploox.weatherdroid.listener.RecyclerItemClickListener;
import com.tooploox.weatherdroid.rest.WeatherClient;
import com.tooploox.weatherdroid.rest.model.search.City;
import com.tooploox.weatherdroid.rest.model.search.CityResponse;
import com.tooploox.weatherdroid.rest.service.parameter.CityParameter;
import com.tooploox.weatherdroid.rest.service.parameter.Parameter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.InjectView;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 *
 * SearchFragment class helps to search for city by post code, ip address, city name.
 *
 * @author Adam Podsiadło
 *
 */

public class SearchFragment extends BaseFragment {
    @InjectView(R.id.rvCitySuggestions) RecyclerView mRvCitySuggestions;

    private HashMap<String, List<City>> mSearchResults;

    private String mQueryKey;

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        setRetainInstance(true);

        initData();
        setListeners();
    }

    @Override
    protected int getFragmentLayout() {
        return R.layout.fragment_city_search;
    } 

    public void findCities(String query) {
        if(mSearchResults != null) {
            if (mSearchResults.containsKey(query)) {
                setCitySuggestions(query);
            } else {
                callSearchAPI(query);
            }
        }
    }

    private void callSearchAPI(final String query) {
        WeatherClient weatherClient = WeatherClient.getInstance();

        HashMap<Parameter, String> parameters = new HashMap<>();
        parameters.put(CityParameter.CITY, query);

        weatherClient.getCities(parameters, new Callback<CityResponse>() {
            @Override
            public void success(CityResponse cityResponse, Response response) {
                if(cityResponse.getResult() != null) {
                    if (cityResponse.getResult().getCityList().size() > 0) {
                        mSearchResults.put(query, cityResponse.getResult().getCityList());

                        setCitySuggestions(query);
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) { }
        });
    }

    private void initData() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        mRvCitySuggestions.setLayoutManager(layoutManager);

        mSearchResults = new HashMap<>();
        mSearchResults.put("", new ArrayList<City>());
    }

    private void setCitySuggestions(String key) {
        mQueryKey = key;
        mRvCitySuggestions.setAdapter(new CitySuggestionAdapter(mSearchResults.get(key)));
    }

    private void setListeners() {
        mRvCitySuggestions.addOnItemTouchListener(new RecyclerItemClickListener(getActivity(), new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(int position) {
                if(mSearchResults.get(mQueryKey).get(position).getAreas().size() > 0) {
                    City city = mSearchResults.get(mQueryKey).get(position);

                    WeatherApp.getBus().post(new SearchCityEventBus(city));
                }
            }
        }));
    }
}
